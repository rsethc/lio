#include <dbInteraction.h>
#include "account_names.h"
void Verify ()
{
	dbLink link;
	link.Begin();
	int total = 0;
	for (int i = 0; i < N_Accounts; i++)
	{
		try
		{
			Sint64 value = link.GetInt(Accounts[i]);
			total += value;
			printf("\"%s\" has $%d\n",Accounts[i],value);
		}
		catch (std::runtime_error err)
		{
			// looks like the account doesn't exist... create it by setting it to starting balance
			link.SetInt(Accounts[i],StartingBalance);
			printf("Setting \"%s\" to $%d\n",Accounts[i],StartingBalance);
			total += StartingBalance;
		};
	};
	link.Commit();
	int expected = N_Accounts * StartingBalance;
	printf("Expected Total: %d\n",expected);
	printf("  Actual Total: %d\n",expected);
};
#define SDL_main main
int main ()
{
	dbInit();
	Verify();
	dbQuit();
	return 0;
};
