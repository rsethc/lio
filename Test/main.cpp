#include <dbInteraction.h>
#include <iostream>

void TestMain ()
{
	dbLink link;

	std::cout<<"connected\n";

	link.Begin();
	try
	{
		link.GetInt("test");
	}
	catch (Sint32 error)
	{
		//if (error == ERR_NAME_NOT_FOUND)
		//{
			std::cout<<"Defaulting to zero.\n";
			link.SetInt("test",0);
		//};
	};
	link.Commit();

	while (true)
	{
		SDL_Delay(100);

		//dbLink link;



		link.Begin();
		Sint64 n = link.GetInt("test");
		n++;
		link.SetInt("test",n);
		link.Commit();

		std::cout << n << '\n';
	};
};

#define SDL_main main
int main ()
{
	dbInit();

	TestMain();

	dbQuit();
	return 0;
};
