data types:
	primitives:
		float 
			(f16, f32, f64, f128)
			simple floating point number
		int 
			(u8, s8, u16, s16, u32, s32, u64, s64)
			simple integer 
	array
		fixed length
		of any type, even array itself
		array(float,64)
			array of 64 floats 
		array(array(float,64),32) 
			array of 32 arrays of 64 floats 
	string 
		arbitrary length up to a maximum, unless 0 is provided for "unlimited" 
		of any type including string itself 
		string(u8,200) 
			max length is 200 u8s, until written to the storage occupied is 0 u8s 
		string(string(u8,200),1000) 
			max length is 1000 strings, of which max length is 200 u8s
		string(f64,0) 
			stored f64s, max length is unlimited
	struct 
		a compound data type with names for each component 
		struct { string(u8,80) name; s64 balance_us_cents; } 
		
struct BankAccount 
{
	string(u8,30) first_name, middle_name, last_name;
	s64 balance_us_cents;
};
string(BankAccount,NO_MAX) Accounts;
access 
{
	none;
};
access Accounts 
{
	any;
};
access Accounts.balance_us_cents
{
	none;
	tagged "transaction";
};
access 

		
		