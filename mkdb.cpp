#include <stdio.h>
#include <string.h>
int main (int argc, char** argv)
{
	char replace = 0;
	if (argc != 2)
	{
		if (argc == 3 && !strcmp(argv[2],"replace"))
		{
			replace = 1;
		}
		else
		{
			puts("Usage: mkdb path/to/dbfile [replace]");
			return -1;
		};
	};

	FILE* file;

	if (!replace)
	{
		file = fopen(argv[1],"rb");
		if (file)
		{
			fclose(file);
			puts("File already exists");
			return -2;
		};
	};

	file = fopen(argv[1],"wb");
	if (!file)
	{
		puts("Cannot open file to write");
		return -3;
	};



	size_t first_control_length = 8;//512;
	size_t data_start = 8+8+first_control_length;
	size_t zero = 0;

	if (fwrite(&first_control_length,1,8,file) != 8) goto FAIL;
		// Interleave layer
		// ctrl segment: segment data length

	if (fwrite(&zero,1,8,file) != 8) goto FAIL;
		// Interleave layer
		// ctrl segment: next segment position = null

	if (fwrite(&zero,1,8,file) != 8) goto FAIL;
		// Transaction layer
		// number of journal entries = 0

	if (fseek(file,data_start,SEEK_SET)) goto FAIL;
		// go to data section

	if (fwrite(&zero,1,8,file) != 8) goto FAIL;
		// allocating layer
		// bysize_root = null

	if (fwrite(&zero,1,8,file) != 8) goto FAIL;
		// allocating layer
		// bypos_root = null

	size_t logical_length = 32; // allocfile header size
	if (fwrite(&logical_length,1,8,file) != 8) goto FAIL;
		// allocating layer
		// logical_length = 32

	if (fwrite(&zero,1,8,file) != 8) goto FAIL;
		// allocating layer
		// primary pointer = null



	fclose(file);
	return 0;

	FAIL:
	puts("Failed to write empty db file.");
	perror("perror: ");
	printf("feof: %d\n",feof(file));
	printf("ferror: %d\n",ferror(file));
	fclose(file);
	return -4;
};
