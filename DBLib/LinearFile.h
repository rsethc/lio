#ifndef LINEARFILE_H_INCLUDED
#define LINEARFILE_H_INCLUDED

#include <cstdio>

class LinearFile
{
	public:
	virtual ~LinearFile ();
	virtual void Read (void* buf, size_t start, size_t len) = 0;
	virtual void Write (void* buf, size_t start, size_t len) = 0;
	virtual size_t CurrentSize () = 0;
	virtual void Flush ();
	void Copy (size_t src_start, size_t dst_start, size_t len);
	void CopyTo (LinearFile* dst, size_t src_start, size_t dst_start, size_t len);
	signed char ContentCompare (void* content, size_t start, size_t len); /// 0 for match, -1 for content < file, 1 for content > file
	inline bool ContentMatch (void* content, size_t start, size_t len) { return !ContentCompare(content,start,len); };
};

#endif // LINEARFILE_H_INCLUDED
