#include "BasicFile.h"

#include <stdexcept>

BasicFile::BasicFile (std::string path, bool create_allowed)
{
	file = fopen(path.c_str(),"r+b");
	if (!file && create_allowed) file = fopen(path.c_str(),"w+b");
	if (!file) throw std::runtime_error("Failed to open \""+path+"\"");

	// Determine file length
	if (fseeko64(file,0,SEEK_END)) goto SEEK_FAILED;
	if ((filesize = ftello64(file)) < 0) goto SEEK_FAILED;
	if (fseeko64(file,0,SEEK_SET)) goto SEEK_FAILED;
	//if (setvbuf(file,NULL,_IONBF,0)) goto FAILED;
	//setbuf(file,NULL);

	printf("raw file size upon opening is %llu bytes\n",(size_t)filesize);

	return;

	SEEK_FAILED:
	fclose(file);
	throw std::runtime_error("Failed initial file size detection \""+path+"\"");
};

BasicFile::~BasicFile ()
{
	//printf("closing file: fd=%llX\n",file);
	fclose(file);
};

void BasicFile::PrintFileErrors ()
{
	printf("fd %llX\n",file);
	perror(NULL);
	printf("ferror: %d\n",ferror(file));
	printf("feof: %d\n",feof(file));
};

void BasicFile::Seek (size_t start)
{
	/** Do *not* skip seeking even if "pos" is the same place.
		Writing appears to fail, if Read to end of file and then Write.
		So, always explicitly seek to the position since that fixes it.
	**/

	if (fseeko64(file,start,SEEK_SET))
	{
		PrintFileErrors();
		throw std::runtime_error("Failed to seek");
	};
};

void BasicFile::Read (void* buf, size_t start, size_t len)
{
	//printf("Reading %llu [%llu]\n",start,len);
	Seek(start);
	if (fread(buf,1,len,file) != len)
	{
		PrintFileErrors();
		throw std::runtime_error("Failed to read");
	};
};

void BasicFile::Write (void* buf, size_t start, size_t len)
{
	//printf("Writing %llu [%llu]\n",start,len);
	Seek(start);
	//int ok;
	//if ((ok = fwrite(buf,1,len,file)) != len)
	if (fwrite(buf,1,len,file) != len)
	{
		//printf("wrote %d bytes\n",ok);
		PrintFileErrors();
		throw std::runtime_error("Failed to write");
	};
	//fflush(file);

	// If this write went past end of file, update filesize.
	start += len; // value of file cursor
	filesize = start > filesize ? start : filesize;
};

void BasicFile::Flush ()
{
	if (fflush(file))
	{
		PrintFileErrors();
		throw std::runtime_error("Failed to flush file buffers");
	};
};

size_t BasicFile::CurrentSize ()
{
    return filesize;
};
