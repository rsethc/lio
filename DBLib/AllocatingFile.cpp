#include "AllocatingFile.h"

AllocatingFile::AllocatingFile (LinearFile* file)
{
    this->file = file;
	//printf("AllocatingFile size is %d bytes\n",sizeof(AllocatingFile));
	if (file->CurrentSize() < 32)
	{
		printf("writing initial allocator header\n");
		size_t zero = 0;
		file->Write(&zero,0,8);
		file->Write(&zero,8,8);
		file->Write(&zero,24,8);

		// Set what has originally been called "logical length" to the size of the header,
		// since implementation changes mean that the addresses no longer are offset by this size.
		zero = 32;
		file->Write(&zero,16,8);
	};
};

size_t AllocatingFile::EndAllocation (size_t len)
{
	printf("end allocation\n");

	size_t logical_length;
	file->Read(&logical_length,16,8);

	size_t header_start = logical_length;
	//printf("write block length to %llu\n",header_start);

	Write(&len,header_start,8); // Write block's length to block header.

	size_t user_start = header_start + 8;

	// Update file logical length.
	logical_length = user_start + len;
	Write(&logical_length,16,8);

	return user_start;
};

void AllocatingFile::BST_Insert (size_t allocation, size_t root_pos, size_t header_offset, size_t (*get_criterion) (AllocatingFile* file, size_t allocation))
{
	size_t left_offset = header_offset;
	size_t right_offset = header_offset + 8;
	size_t downptr_offset = header_offset + 16;



	//printf("BST Insert");

	size_t allocation_criterion = get_criterion(this,allocation);

	// clear allocation's left and right pointers
	size_t zero = 0;
	Write(&zero,allocation + left_offset,8);
	Write(&zero,allocation + right_offset,8);

	size_t downptr = root_pos; // address of bysize_root
	while (true)
	{
		//printf(".");
		size_t node_addr;
		Read(&node_addr,downptr,8);

		if (node_addr)
		{
			// decide which way to go downward
			size_t node_criterion = get_criterion(this,node_addr);
			if (allocation_criterion < node_criterion)
			{
				// go left
				downptr = node_addr + left_offset; // bysize_left
			}
			else
			{
				// go right
				downptr = node_addr + right_offset; // bysize_right
			};
		}
		else
		{
			// the downptr doesn't point to anything, so put allocation here.

			/* overwrite downptr to point to allocation */
			Write(&allocation,downptr,8);

			/* overwrite allocation's bysize_parent */
			Write(&downptr,allocation + downptr_offset,8);

			//printf("#\n");
			break;
		};
	};
};

void AllocatingFile::BST_Remove (size_t allocation, size_t header_offset)
{
	size_t left_offset = header_offset;
	size_t right_offset = header_offset + 8;
	size_t downptr_offset = header_offset + 16;



	size_t bysize_left,bysize_right,bysize_downptr;
	Read(&bysize_left,allocation + left_offset,8);
	Read(&bysize_right,allocation + right_offset,8);
	Read(&bysize_downptr,allocation + downptr_offset,8);
	if (bysize_left && bysize_right)
	{
		// find minimum value from right subtree, and replace removed with it
		// aka leftmost value
		size_t next_node = bysize_right;
		size_t node_addr;
		do // First time guaranteed bysize_right exists.
		{
			next_node = node_addr;
			Read(&node_addr,next_node + left_offset,8);
		}
		while (node_addr);

		// make the previous parent of min-value point to
		// the right child of the node we are moving (if there was one).
		size_t previous_rightchild;
		Read(&previous_rightchild,next_node + right_offset,8);

		if (next_node == bysize_right)
		{
			// bysize_right *is* next_node
			// bysize_right itself moved up
			bysize_right = previous_rightchild;
		}
		else
		{
			size_t minval_downptr;
			Read(&minval_downptr,next_node + downptr_offset,8);

			// don't bother overwriting downptr if downptr is from the node we remove
			// otherwise do, so that there is not a cyclic reference left over
			size_t zero = 0;
			Write(&zero,minval_downptr,8);
		};

		// now do the replacement
		Write(&bysize_left,next_node + left_offset,8);
		Write(&bysize_right,next_node + right_offset,8);
		Write(&bysize_downptr,next_node + downptr_offset,8);
		Write(&next_node,bysize_downptr,8);
	}
	else if (bysize_left || bysize_right)
	{
		bysize_left = bysize_left ? bysize_left : bysize_right;
		// bysize_left now points at whichever was not null

		// point the downptr to the child, point the child to downptr
		Write(&bysize_left,bysize_downptr,8);
		Write(&bysize_downptr,bysize_left + downptr_offset,8);
	}
	else
	{
		// set the downptr to null
		size_t zero = 0;
		Write(&zero,bysize_downptr,8);
	};
};

size_t GetAllocationPos (AllocatingFile* file, size_t allocation)
{
	return allocation;
};

size_t GetAllocationSize (AllocatingFile* file, size_t allocation)
{
	size_t size;
	file->Read(&size,allocation,8);
	return size;
};

void AllocatingFile::BySizeInsert (size_t allocation)
{
	BST_Insert(allocation,0,8,GetAllocationSize);
};

void AllocatingFile::BySizeRemove (size_t allocation)
{
	BST_Remove(allocation,8);
};

void AllocatingFile::ByPosInsert (size_t allocation)
{
	BST_Insert(allocation,8,32,GetAllocationPos);
};

void AllocatingFile::ByPosRemove (size_t allocation)
{
	BST_Remove(allocation,32);
};

size_t AllocatingFile::TakeWholeAllocation (size_t allocation)
{
	printf("exact allocation\n");
	BySizeRemove(allocation);
	ByPosRemove(allocation);
	return allocation;
};

size_t AllocatingFile::TakePartialAllocation (size_t allocation, size_t len)
{
	printf("partial allocation\n");
	// temporarily do not subdivide the free space
	return TakeWholeAllocation(allocation);
};

size_t AllocatingFile::Allocate (size_t len)
{
	if (len == 0) return 0; // Immediately return NULL for zero-length allocations.

	if (len < 48) len = 48; // large enough to union with the free space info structure
	//printf("ALLOCATE\n");

	// Search through free spaces.
	size_t node_addr;
	Read(&node_addr,0,8); // read from bysize_root
	size_t suitable_addr = 0;
	size_t suitable_size;
	while (node_addr)
	{
		printf("inspecting node %llu\n",node_addr);
		size_t node_len;
		Read(&node_len,node_addr,8);
		printf("node is %llu bytes\n",node_len);
		if (node_len < len)
		{
			printf("too small, moving right\n");
			// This is not acceptable, look for larger spaces.
			size_t bysize_right;
			Read(&bysize_right,node_addr + 16,8);
			node_addr = bysize_right;
		}
		else if (node_len > len)
		{
			printf("acceptable, moving left though\n");
			// This is acceptable, but also look for smaller spaces.
			suitable_addr = node_addr;
			suitable_size = node_len;
			size_t bysize_left;
			Read(&bysize_left,node_addr + 8,8);
			node_addr = bysize_left;
		}
		else
		{
			printf("perfect\n");
			// This is perfect!
			//suitable_addr = node_addr;
			//break;
			return TakeWholeAllocation(node_addr);
		};
	};
	if (suitable_addr)
	{
		// only fragment the allocation
		// if there is enough space
		// for the remaining free space's
		// header (8 byte length value) and
		// info structure (48 bytes).
		// otherwise just take the whole thing.
		if (len + 56 >= suitable_size) TakePartialAllocation(suitable_addr,len);
		else return TakeWholeAllocation(suitable_addr);
	}
	else return EndAllocation(len);
};

#include <stdexcept>

size_t AllocatingFile::GetAdjacentBefore (size_t allocation)
{
	size_t node_addr;
	Read(&node_addr,8,8); // read from bypos_root
	while (node_addr)
	{
		size_t node_len;
		Read(&node_len,node_addr,8);
		if (node_addr + 8 + node_len == allocation) return node_addr; // Found it!
		else if (node_addr + 8 + node_len < allocation)
		{
			// Go right.
			size_t bypos_right;
			Read(&bypos_right,node_addr + 40,8);
			node_addr = bypos_right;
		}
		else if (node_addr > allocation)
		{
			// Go left.
			size_t bypos_left;
			Read(&bypos_left,node_addr + 32,8);
			node_addr = bypos_left;
		}
		else
		{
			// Heap corruption!
			throw std::runtime_error("Heap corruption detected (before-adjacent check).");
		};
	};
	return 0;
};

size_t AllocatingFile::GetAdjacentAfter (size_t allocation)
{
	// the input address doesn't have to be currently free, only have a valid size.

	size_t allocation_len;
	Read(&allocation_len,allocation,8);
	size_t searchfor = allocation + 8 + allocation_len;

	size_t node_addr;
	Read(&node_addr,8,8); // read from bypos_root
	while (node_addr)
	{
		if (node_addr == searchfor)
		{
			return node_addr; // Found it!
		}
		else if (node_addr > searchfor)
		{
			// Go left.
			size_t bypos_left;
			Read(&bypos_left,node_addr + 32,8);
			node_addr = bypos_left;
		}
		else if (node_addr < allocation)
		{
			// Go right.
			size_t bypos_right;
			Read(&bypos_right,node_addr + 40,8);
			node_addr = bypos_right;
		}
		else
		{
			// Heap corruption!
			throw std::runtime_error("Heap corruption detected (after-adjacent check).");
		};
	};
	return 0;
};

void AllocatingFile::Release (size_t address)
{
	if (address == 0) return; // Freeing exactly NULL is allowed, since NULL will be produced by Allocate(0).

	//printf("RELEASE %llX\n",address);

	if (address < 40) // The space header would overlap with the file header! Invalid.
	{
		throw std::runtime_error("Invalid address was passed to Release function");
	};

	address -= 8; // point to allocation instead of the user's data

	size_t space_length;
	Read(&space_length,address,8);


	size_t logical_length;
	file->Read(&logical_length,16,8);


	// special case: release adjacent to end of file
	size_t goes_to = address + 8 + space_length;
	if (goes_to > logical_length)
		throw std::runtime_error("Heap corruption: allocation end > logical end");
	if (goes_to == logical_length)
	{
		//printf("special case\n");
		//printf("get adjacent before: ");
		size_t before = GetAdjacentBefore(address);
		//printf("%llX\n",before);
		if (before)
		{
			// remove before and bring logical_length back once again
			BySizeRemove(before);
			ByPosRemove(before);
			logical_length = before;
		}
		else
		{
			logical_length = address;
		};
		Write(&logical_length,16,8);

		return;
	};

//	printf("get adjacent before: ");
	size_t before = GetAdjacentBefore(address);
//	printf("%llX\nget adjacent after: ",before);
	size_t after = GetAdjacentAfter(address);
//	printf("%llX\n",after);

	if (before && after)
	{
		// remove before, remove after, update before's length, re-add before.

		BySizeRemove(after);
		ByPosRemove(after);

		size_t before_len,after_len;
		Read(&before_len,before,8);
		Read(&after_len,after,8);
		before_len = before_len + 8 + space_length + 8 + after_len;
		Write(&before_len,before,8);

		BySizeRemove(before);
		BySizeInsert(before_len);
	}
	else if (before)
	{
		// remove before, update before's length, re-add before.

		size_t before_len;
		Read(&before_len,before,8);
		before_len += 8 + space_length;
		Write(&before_len,before,8);

		BySizeRemove(before);
		BySizeInsert(before);
	}
	else if (after)
	{
		// remove after, update here's length, add here.

		BySizeRemove(after);
		ByPosRemove(after);

		size_t after_len;
		Read(&after_len,after,8);
		space_length += 8 + after_len;
		Write(&space_length,address,8);

		BySizeInsert(address);
		ByPosInsert(address);
	}
	else
	{
		// add here.

		BySizeInsert(address);
		ByPosInsert(address);
	};
};

void AllocatingFile::SetPrimaryPointer (size_t primary_pointer)
{
	Write(&primary_pointer,24,8);
};
size_t AllocatingFile::GetPrimaryPointer ()
{
	size_t primary_pointer;
	Read(&primary_pointer,24,8);
	return primary_pointer;
};

size_t AllocatingFile::Reallocate (size_t address, size_t new_len)
{
	if (address == 0) return Allocate(new_len);
	if (new_len == 0)
	{
		/*if (address)*/ Release(address);
		return 0;
	};

	if (address < 40) // The space header would overlap with the file header! Invalid.
	{
		throw std::runtime_error("Invalid address was passed to Reallocate function");
	};

	size_t allocation = address - 8;
	size_t old_len;
	Read(&old_len,allocation,8);
	if (old_len == new_len) return address;

	size_t goes_to = address + old_len;
	size_t logical_length;
	Read(&logical_length,16,8);
	if (goes_to > logical_length)
		throw std::runtime_error("Heap corruption: allocation end > logical end");
	if (goes_to == logical_length)
	{
		size_t diff = new_len - old_len;
		logical_length += diff;
		Write(&logical_length,16,8);
		Write(&new_len,allocation,8);
		return address;
	};

	size_t after = GetAdjacentAfter(address);
	if (new_len > old_len)
	{
		if (after)
		{
			size_t after_len;
			Read(&after_len,after,8);
			size_t combined = old_len + 8 + after_len;
			if (combined >= new_len)
			{
				/* Can reallocate. */
				size_t remaining = combined - new_len;
				BySizeRemove(after);
				ByPosRemove(after);
				if (remaining >= 56)
				{
					size_t diff = new_len - old_len;
					after += diff;
					after_len -= diff;
					Write(&after_len,after,8);
					BySizeInsert(after);
					ByPosInsert(after);
				}
				else
				{
					new_len = combined;
				};
				Write(&new_len,allocation,8);
				return address;
			};
		};
		// Can't expand in-place.
		size_t copy_len = old_len < new_len ? old_len : new_len; // min
		size_t new_addr = Allocate(new_len);
		file->Copy(address,new_addr,copy_len);
		Release(address);
		return new_addr;
	}
	else
	{
		if (after)
		{
			// Increase length of the adjacent free space.
			// The position of its beginning moves backward.
			size_t diff = old_len - new_len;
			size_t after_len;
			Read(&after_len,after,8);
			BySizeRemove(after);
			ByPosRemove(after);
			after_len += diff;
			after -= diff;
			Write(&after_len,after,8);
			BySizeInsert(after);
			ByPosInsert(after);

			// Resize the allocation with shorter length.
			Write(&new_len,allocation,8);
		}
		else if (old_len >= new_len + 56)
		{
			// Create new free space from the space at the end.
			after = allocation + 8 + new_len;
			size_t after_len = old_len - new_len;
			Write(&after_len,after,8);
			BySizeInsert(after);
			ByPosInsert(after);

			// Resize the allocation with shorter length.
			Write(&new_len,allocation,8);
		};
		return address;
	};
	// A note about allocation length being longer than "new_len":
	// Yes technically this could leave some amount of memory allocated
	// when the application doesn't care about it.
	// However it is a maximum of 55 bytes wasted, and recoverable.
	// Anyway it is much better than copying to relocate the region,
	// or to waste 8 extra bytes on every allocation
	// for a secondary 'logical size' value.
};
