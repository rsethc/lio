#ifndef ALLOCATINGFILE_H_INCLUDED
#define ALLOCATINGFILE_H_INCLUDED

#include "TransactionalFile.h"

class AllocatingFile
{
    LinearFile* file;

	void BST_Remove (size_t allocation, size_t header_offset);
	void BST_Insert (size_t allocation, size_t root_pos, size_t header_offset, size_t (*get_criterion) (AllocatingFile* file, size_t allocation));
	void BySizeInsert (size_t allocation);
	void BySizeRemove (size_t allocation);
	void ByPosInsert (size_t allocation);
	void ByPosRemove (size_t allocation);
	size_t TakeWholeAllocation (size_t allocation);
	size_t TakePartialAllocation (size_t allocation, size_t len);
	size_t EndAllocation (size_t len);
	size_t GetAdjacentBefore (size_t allocation);
	size_t GetAdjacentAfter (size_t allocation);

	public:

	inline void Read (void* buf, size_t start, size_t len) { file->Read(buf,start,len); };
	inline void Write (void* buf, size_t start, size_t len) { file->Write(buf,start,len); };

	AllocatingFile (LinearFile* file);
	//~AllocatingFile ();
	size_t Allocate (size_t len);
	void Release (size_t address);
	size_t Reallocate (size_t address, size_t new_len);
	//inline void Commit () { TransactionalFile::Commit(); };
	void Revert (); // overrides parent function because also invalidates cache
	void SetPrimaryPointer (size_t primary_pointer);
	size_t GetPrimaryPointer ();

    friend size_t GetAllocationPos (AllocatingFile* file, size_t allocation);
    friend size_t GetAllocationSize (AllocatingFile* file, size_t allocation);
};

#endif // ALLOCATINGFILE_H_INCLUDED
