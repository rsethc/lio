#include "LinearFile.h"

LinearFile::~LinearFile ()
{
	// This block is intentionally empty.
};

void LinearFile::Flush ()
{
	// This block is intentionally empty.
};

void LinearFile::Copy (size_t src_start, size_t dst_start, size_t len)
{
	CopyTo(this,src_start,dst_start,len);
};

void LinearFile::CopyTo (LinearFile* dst, size_t src_start, size_t dst_start, size_t len)
{
	constexpr size_t bufsize = 1024;
	char buf [bufsize];
	while (len)
	{
		size_t chunk = bufsize < len ? bufsize : len;
		len -= chunk;
		Read(buf,src_start,chunk);
		src_start += chunk;
		dst->Write(buf,dst_start,chunk);
		dst_start += chunk;
	};
};

signed char LinearFile::ContentCompare (void* content, size_t start, size_t len) /// 0 for match, -1 for content < file, 1 for content > file
{
	constexpr size_t bufsize = 1024;
	char buf [bufsize];
	while (len)
	{
		size_t chunk = bufsize < len ? bufsize : len;
		len -= chunk;
		Read(buf,start,chunk);
		for (size_t i = 0; i < chunk; i++)
		{
			char in_memory = ((char*)content)[i];
			char in_file = buf[i];
			if (in_memory != in_file)
			{
				if (in_memory > in_file) return 1;
				else return -1;
			};
		};
		start += chunk;
		content += chunk;
	};
	return 0;
};
