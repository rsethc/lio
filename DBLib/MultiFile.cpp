#include "MultiFile.h"
#include <stdlib.h>
#include <stdexcept>



Region* ArraysFileArray::NewRegion ()
{
    if (regions_count >= regions_capacity)
        regions_buf = realloc(regions_buf,sizeof(Region) * (regions_capacity <<= 1));
    return regions_buf + regions_count++;
};

void ArraysFileArray::Expand ()
{
    size_t last_ptr,new_size,vstart;
    if (regions_count)
    {
        Region* last_region = regions_buf + regions_count - 1;
        last_ptr = last_region->physical_start - 8;
        new_size = last_region->length << 1;
        vstart = last_region->virtual_start + last_region->length;
    }
    else
    {
        last_ptr = 24 + array_id * 16;
        new_size = host->starting_size;
        vstart = 0;
    };
	printf("array %d of %d: expand %llu [%llu]\n",array_id,host->n_arrays,host->frontier,new_size);

    Region* region = NewRegion();
    size_t real_start = host->frontier;
    region->physical_start = real_start + 8;
    region->virtual_start = vstart;
    region->length = new_size;

    host->frontier = region->physical_start + region->length;
    host->underlying->Write(&host->frontier,8,8);
    host->underlying->Write(&real_start,last_ptr,8);
    size_t zero = 0;
    host->underlying->Write(&zero,real_start,8);

    return region;
};

void ArraysFileArray::Read (void* buf, size_t start, size_t len)
{
	printf("array %d of %d: read %llu [%llu]\n",array_id,host->n_arrays,start,len);

	/*
		(1) binary search for the first region
		(2) until finished:
			(A) if no region, give up (leave junk data in buffer)
			(B) read/write within region
			(C) advance to next region

	*/

    size_t region_id;
    size_t look_start = 0;
    size_t look_range = regions_count;
    while (true)
    {
    	if (!look_range) return;
        size_t look_mid = look_start + (look_range >> 1);
        Region* region = regions_buf + look_mid;
        if (region->virtual_start > start)
        {
            // Go left.
            look_range = look_mid - look_start;
        }
        else if (region->virtual_start + region->length <= start)
        {
            // Go right.
            look_range -= look_mid - look_start + 1;
            look_start = look_mid + 1;
            if (look_start >= regions_count) return;
        }
        else
        {
            // This one.
            region_id = look_mid;
            break;
        };
    };

	while (len)
	{
        Region* region = regions_buf + region_id;
        size_t chunk_len = len;
        size_t chunk_start = start - region->virtual_start;
        if (chunk_start + chunk_len >= region->length)
            chunk_len = region->length - chunk_start;

        host->underlying->Read(buf,region->physical_start + chunk_start,chunk_len);

        buf += chunk_len;
        start += chunk_len;
        len -= chunk_len;

        if (++region_id >= regions_count) return;
	};
};

void ArraysFileArray::Write (void* buf, size_t start, size_t len)
{
	printf("array %d of %d: write %llu [%llu]\n",array_id,host->n_arrays,start,len);

	/*
		(1) binary search for the first region
		(2) until finished:
			(A) if no region, expand to a new region
			(B) read/write within region
			(C) advance to next region
	*/

    size_t region_id;
    size_t look_start = 0;
    size_t look_range = regions_count;
    if (!look_range)
	{
		Expand();
		look_range++;
	};
	if (!look_range) Expand();
    while (true)
    {
        size_t look_mid = look_start + (look_range >> 1);
        Region* region = regions_buf + look_mid;
        if (region->virtual_start > start)
        {
            // Go left.
            look_range = look_mid - look_start;
        }
        else if (region->virtual_start + region->length <= start)
        {
            // Go right.
            look_range -= look_mid - look_start + 1;
            look_start = look_mid + 1;
            if (look_start >= regions_count)
            {
                Expand();
                look_range++;
            };
        }
        else
        {
            // This one.
            region_id = look_mid;
            break;
        };
    };

	while (len)
	{
        Region* region = regions_buf + region_id;
        size_t chunk_len = len;
        size_t chunk_start = start - region->virtual_start;
        if (chunk_start + chunk_len >= region->length)
            chunk_len = region->length - chunk_start;

        host->underlying->Write(buf,region->physical_start + chunk_start,chunk_len);

        buf += chunk_len;
        start += chunk_len;
        len -= chunk_len;

        if (++region_id >= regions_count) Expand();
	};

    // Update file size if it has increased.
    size_t end_addr = start + len;
    if (end_addr >= virtual_filesize)
    {
        virtual_filesize = end_addr;
        host->underlying->Write(&virtual_filesize,24 + array_id * 16 + 8,8);
    };
};

size_t ArraysFileArray::CurrentSize ()
{
    return virtual_filesize;
};



ArraysFileHost::ArraysFileHost (LinearFile* underlying, size_t n_arrays, size_t starting_size)
{
	this->underlying = underlying;
	this->n_arrays = n_arrays;
	size_t header_size = 24 + n_arrays * 16;
    if (underlying->CurrentSize() >= 24)
    {
        underlying->Read(&n_arrays,0,8);
        underlying->Read(&frontier,8,8);
        underlying->Read(&starting_size,16,8); // Just ignore any mismatch and use the file's value.
        if (n_arrays != this->n_arrays)
            throw std::runtime_error("stream count to ArraysFileHost constructor differs from file value");
        if (frontier < header_size || underlying->CurrentSize() < header_size)
            throw std::runtime_error("ArraysFileHost header is corrupted");
    }
    else
    {
    	printf("writing initial arrays header\n");
        frontier = header_size;
        underlying->Write(&n_arrays,0,8);
        underlying->Write(&frontier,8,8);
        underlying->Write(&starting_size,16,8);
        size_t zero = 0;
        for (size_t i = 0; i < n_arrays; i++)
        {
            underlying->Write(&zero,24 + i * 16,8);
            underlying->Write(&zero,24 + i * 16 + 8,8);
        };
    };
	this->starting_size = starting_size;

    // Load the regions for each array.
	arrays = new ArraysFileArray [n_arrays];
	for (size_t i = 0; i < n_arrays; i++)
	{
		ArraysFileArray* array = arrays + i;
		array->host = this;
		array->array_id = i;
		array->regions_count = 0;
		array->regions_capacity = 1;
		array->regions_buf = malloc(sizeof(Region) * array->regions_capacity);
		size_t array_header_pos = 24 + i * 16;
		underlying->Read(&array->virtual_filesize,array_header_pos + 8,8);
        size_t region_size = starting_size;
        size_t virtual_pos = 0;

		size_t region_start;
		underlying->Read(&region_start,array_header_pos,8);
		while (region_start)
        {
            Region* region = array->NewRegion();
            region->length = region_size;
            region->physical_start = region_start + 8;
            region->virtual_start = virtual_pos;

            virtual_pos += region_size;
            region_size <<= 1;

            underlying->Read(&region_start,region_start,8);
        };
	};

	/*underlying->Read(&regions_count);
	regions_capacity = 1;
	while (regions_capacity < regions_count) regions_capacity <<= 1;
	regions_buf = */
};

ArraysFileHost::~ArraysFileHost ()
{
	for (size_t i = 0; i < n_arrays; i++)
	{
		ArraysFileArray* array = arrays + i;
		free(array->regions_buf);
	};
	delete[] arrays;
};
