#include "TransactionalFile.h"
#include <stdlib.h>

/**
	Future optimization: do not preserve (and do not overwrite) write sections which match the current data.
**/

TransactionalFile::TransactionalFile (LinearFile* underlying) : arrays_host(underlying,2)
{
	Revert();
	preserved_regions_memory = malloc(sizeof(PreservedRegion) * preserved_regions_capacity);
};

TransactionalFile::~TransactionalFile ()
{
	free(preserved_regions_memory);
};

void TransactionalFile::ResetPreservedRegions ()
{
	preserved_regions_root = -1;
	preserved_regions_usage = 0;
};

void TransactionalFile::InsertPreservedRegion (size_t start, size_t len)
{
	if (preserved_regions_usage >= preserved_regions_capacity)
	{
		preserved_regions_capacity <<= 1;
		preserved_regions_memory = realloc(preserved_regions_memory,sizeof(PreservedRegion) * preserved_regions_capacity);
	};
	LocalIndex newnode_index = preserved_regions_usage++;
	PreservedRegion* newnode = preserved_regions_memory + newnode_index;
	newnode->start = start;
	newnode->len = len;
	//printf("incoming start = %i, incoming len = %i\n",(int)start,(int)len);
	//printf("node start = %i, node len = %i\n",newnode->start,newnode->len);
	newnode->left = -1;
	newnode->right = -1;

	LocalIndex* downward_ptr = &preserved_regions_root;
	LocalIndex parent_idx = -1; // The root node does not have a parent
	while (*downward_ptr >= 0)
	{
		// Advance to either side
		parent_idx = *downward_ptr;
		PreservedRegion* parent = preserved_regions_memory + parent_idx;
		if (start < parent->start) downward_ptr = &parent->left;
		else downward_ptr = &parent->right;
	};
	*downward_ptr = newnode_index;
	newnode->parent = parent_idx;
};

void TransactionalFile::PreserveOriginalRegion (size_t start, size_t len)
{
	if (!len) return; // Under no circumstances should zero-length regions be added to the tree.

	printf("\t%llu [%llu]\n",start,len);

	ControlWrite(&start,journal_next_write_pos,8);
	journal_next_write_pos += 8;
	ControlWrite(&len,journal_next_write_pos,8);
	journal_next_write_pos += 8;
	DataToControl(start,journal_next_write_pos,len);
	journal_next_write_pos += len;

	journal_write_count++;
	ControlWrite(&journal_write_count,0,8);

	InsertPreservedRegion(start,len);
};

LocalIndex TransactionalFile::LowestOverlapRegion (size_t start, size_t len)
{
	LocalIndex node_index = preserved_regions_root;
	if (node_index >= 0)
	{
		while (true)
		{
			PreservedRegion region = preserved_regions_memory[node_index];
			if (start < region.start && region.left >= 0)
				// our region starts before this region starts, something may be lower
				node_index = region.left;
			else if (start >= region.start + region.len && region.right >= 0)
				// our region starts after this region ends, so start with something higher
				node_index = region.right;
			else
				// this region is the lowest one that overlaps
				break;
		};
	};
	return node_index;
};

LocalIndex TransactionalFile::NextHigher (LocalIndex from)
{
	PreservedRegion lower = preserved_regions_memory[from];
	if (lower.right >= 0) return lower.right;
	else
	{
		LocalIndex parent_idx = lower.parent;
		while (parent_idx >= 0)
		{
			PreservedRegion parent = preserved_regions_memory[parent_idx];
			if (parent.start > lower.start) break;
			parent_idx = parent.parent;
		};
		return parent_idx;
	};
};

void TransactionalFile::Preserve (size_t start, size_t len)
{
	// None of this region existed originally to be preserved.
	if (start >= transaction_begin_filesize) return;

	size_t goes_to = start + len;
	if (goes_to > transaction_begin_filesize)
		// Only some of this region existed originally to be preserved.
		len -= goes_to - transaction_begin_filesize;

	printf("preserve %llu [%llu]\n",start,len);

	LocalIndex already_preserved_idx = LowestOverlapRegion(start,len);
	while (already_preserved_idx >= 0)
	{
		PreservedRegion already_preserved = preserved_regions_memory[already_preserved_idx];
		if (already_preserved.start <= start)
		{
			size_t goes_to = already_preserved.start + already_preserved.len;
			if (goes_to > start)
			{
				size_t overlap = goes_to - start;
				start += overlap;
				if (len > overlap) len -= overlap;
				else
				{
					len = 0;
					break;
				};
			};
		}
		else
		{
			if (already_preserved.start >= start + len) break;
			size_t local_len = already_preserved.start - start;
			PreserveOriginalRegion(start,local_len);
			if (already_preserved.start + already_preserved.len >= start + len) len = 0;
			else
			{
				size_t prev_start = start;
				start = already_preserved.start + already_preserved.len;
				len -= start - prev_start;
			};
		};
		already_preserved_idx = NextHigher(already_preserved_idx);
	};
	if (len) PreserveOriginalRegion(start,len);
};

void TransactionalFile::Read (void* buf, size_t start, size_t len)
{
	DataRead(buf,start,len);
};

void TransactionalFile::Write (void* buf, size_t start, size_t len)
{
	Preserve(start,len);
	DataWrite(buf,start,len);
};

size_t TransactionalFile::CurrentSize ()
{
    return arrays_host.arrays[1].CurrentSize();
};

void TransactionalFile::Commit ()
{
	printf("commit\n");
	journal_write_count = 0;
	ControlWrite(&journal_write_count,0,8);
	arrays_host.Flush();
	journal_next_write_pos = 8;
	ResetPreservedRegions();
	transaction_begin_filesize = LogicalDataFilesize();
};

void TransactionalFile::Revert ()
{
	if (arrays_host.arrays[0].CurrentSize() >= 8) // Newly created file won't have a changes value.
	{
		size_t changes;
		ControlRead(&changes,0,8);
		printf("revert %llu regions\n",changes);
		size_t pos = 8;
		while (changes--)
		{
			size_t start,len;
			ControlRead(&start,pos,8);
			pos += 8;
			ControlRead(&len,pos,8);
			pos += 8;
			printf("\%llu [%llu]\n",start,len);
			ControlToData(pos,start,len);
			pos += len;
		};
	}
	else printf("not reverting because file is new\n");
	Commit();
};
