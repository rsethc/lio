#ifndef TRANSACTIONALFILE_H_INCLUDED
#define TRANSACTIONALFILE_H_INCLUDED

#include <stdlib.h>
#include <cstdint>
typedef int64_t LocalIndex;

struct PreservedRegion
{
	// These are *physical* file addresses.

	// Data
	size_t start;
	size_t len;

	// Indices in TransactionalFile::preserved_regions_memory
	LocalIndex left,right,parent;
};

#include "MultiFile.h"

class TransactionalFile : public LinearFile
{
    ArraysFileHost arrays_host;

	size_t journal_write_count;
	size_t journal_next_write_pos;

	size_t preserved_regions_capacity = 16;
	PreservedRegion* preserved_regions_memory; // initialized in constructor
	size_t preserved_regions_usage;
	LocalIndex preserved_regions_root; // Index < 0 means does not exist
	void ResetPreservedRegions ();
	void InsertPreservedRegion (size_t start, size_t len);
	void PreserveOriginalRegion (size_t start, size_t len);
	LocalIndex LowestOverlapRegion (size_t start, size_t len);
	LocalIndex NextHigher (LocalIndex from);
	size_t transaction_begin_filesize;
	void Preserve (size_t start, size_t len);

	// Emulation of the InterleavedFile interface.
	inline void ControlRead (void* buf, size_t start, size_t len)
        { arrays_host.arrays[0].Read(buf,start,len); };
	inline void ControlWrite (void* buf, size_t start, size_t len)
        { arrays_host.arrays[0].Write(buf,start,len); };
	inline void DataRead (void* buf, size_t start, size_t len)
        { arrays_host.arrays[1].Read(buf,start,len); };
	inline void DataWrite (void* buf, size_t start, size_t len)
        { arrays_host.arrays[1].Write(buf,start,len); };
	inline void DataToControl (size_t read_pos, size_t write_pos, size_t len)
        { arrays_host.arrays[1].CopyTo(arrays_host.arrays + 0,read_pos,write_pos,len); };
	inline void ControlToData (size_t read_pos, size_t write_pos, size_t len)
        { arrays_host.arrays[0].CopyTo(arrays_host.arrays + 1,read_pos,write_pos,len); };
    inline size_t LogicalDataFilesize ()
        { return arrays_host.arrays[1].CurrentSize(); };


	public:

	TransactionalFile (LinearFile* underlying);
	~TransactionalFile ();

	virtual void Read (void* buf, size_t start, size_t len);
	virtual void Write (void* buf, size_t start, size_t len);
	virtual size_t CurrentSize ();

	void Commit ();
	void Revert ();

	//inline void ProduceStreamDumps (std::string out_path) { InterleavedFile::ProduceStreamDumps(out_path); };
};

#endif // TRANSACTIONALFILE_H_INCLUDED

