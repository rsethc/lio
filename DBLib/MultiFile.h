#ifndef MULTIFILE_H_INCLUDED
#define MULTIFILE_H_INCLUDED

#include "LinearFile.h"



struct Region
{
	size_t virtual_start,physical_start,length;
};



class ArraysFileHost;



class ArraysFileArray : public LinearFile
{
    friend class ArraysFileHost;

	ArraysFileHost* host;
	Region* regions_buf;
	size_t regions_count;
	size_t regions_capacity;
	size_t array_id;
	size_t virtual_filesize;
	// The regions are loaded when the file is opened.
	// On disk it's just a linked list from one region to the next.
	Region* NewRegion (); // Just gets a new Region object, doesn't do file I/O.
	void Expand (); // Actually expands into a new region.

	public:
	virtual void Read (void* buf, size_t start, size_t len);
	virtual void Write (void* buf, size_t start, size_t len);
	virtual size_t CurrentSize ();

	// void ProduceStreamDump (std::string out_path);
};



class ArraysFileHost
{
    friend class ArraysFileArray;

	size_t n_arrays;
	size_t frontier; // position of the first byte not belonging to an array yet.
	LinearFile* underlying;
	size_t starting_size;

	public:
	ArraysFileHost (LinearFile* underlying, size_t n_arrays, size_t starting_size = 1024);
	~ArraysFileHost ();

	inline void Flush () { underlying->Flush(); };

	ArraysFileArray* arrays; // Cannot change after file creation.

	// void ProduceStreamDumps (std::string out_path);
};



#endif // MULTIFILE_H_INCLUDED
