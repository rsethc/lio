#ifndef BASICFILE_H_INCLUDED
#define BASICFILE_H_INCLUDED

#include "LinearFile.h"
#include <string>

class BasicFile : public LinearFile
{
	FILE* file;
	off64_t filesize;
	void Seek (size_t start);
	void PrintFileErrors ();

	public:
	BasicFile (std::string path, bool create_allowed = true);
	virtual ~BasicFile ();
	virtual void Read (void* buf, size_t start, size_t len);
	virtual void Write (void* buf, size_t start, size_t len);
	virtual size_t CurrentSize ();
	virtual void Flush ();
};

#endif // BASICFILE_H_INCLUDED
