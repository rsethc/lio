#include <smh/smh.h>
#include <dbInteraction.h>
#include <iostream>
/*
char* PageRankingsScriptSrc = R"(

// Data types
struct PageViews
{
	string[u8:infinite] page;
	u64 views default 0;
};

db
{
	// What must be in the database for the script to work.
	PageViews page_views sorted views;

	// In this script we specify page_views has to be sorted by views
	// so that "decreasing views" has good performance.
};
in
{
	// No inputs to this script.
};
out
{
	// Output of the script when it is finished.
	PageViews page_views;
};

// Script code
out.page_views = db.page_views sorted views decreasing;

)";


char* PageVisitScriptSrc = R"(

// Data types
struct PageViews
{
	string[u8:infinite] page;
	u64 views default 0;
};

db
{
	// What must be in the database for the script to work.
	PageViews page_views sorted page;

	// In this script we specify page_views has to have sorting by page
	// so that "search" is a binary search with nice performance.
};
in
{
	// What will come in to the script when it starts.
	string[u8:infinite] page;
};
out
{
	// No outputs from this script when it's done.
};

// Script code
PageViews entry = search db.page_vews for page == in.page;
if (entry == null)
{
	entry = newentry db.page_views
	{
		page = in.page;
	};
};
entry.views = entry.views + 1;

)";

void TrackPageVisit (std::string page_path)
{
	dbLink link;
	dbScript* script = link.NewScript(PageVisitScriptSrc);
	dbScriptExec* exec = script.Exec();
	exec->SetToString("page",page_path.c_str());
};
*/

#include <mime.h>
extern SDL_atomic_t ListenersActive;
extern volatile char ServerShouldDie;

void TrackPageVisit (std::string path)
{
	dbLink DataLink;
	DataLink.Begin();
	try
	{
		int64_t views = DataLink.GetInt(path.c_str());
		views++;
		DataLink.SetInt(path.c_str(),views);
		printf("visits: %lld\n",views);
	}
	catch (std::runtime_error err)
	{
		// this should be cleaner in the future but for now
		// just assume this exception means there was no key found
		// so, create the key and default it to 1 (first page-view).
		DataLink.SetInt(path.c_str(),1);
		printf("defaulting to 1\n");
	};
	DataLink.Commit();
};

void HandleRequest (Client* client)
{
	std::string fullpath = "DocumentRoot/"; // Base directory.
	fullpath += client->path;

	// If the path is a directory, append a default index file name.
	char final_char = fullpath[fullpath.length() - 1];
	if (final_char == '/' || final_char == '\\')
		fullpath += "index.html";

	FILE* file = fopen(fullpath.c_str(),"rb");
	if (file)
	{
		Status200(client,DeduceMimeType(client->path));
		HTTP_SendFile(client,file);

		TrackPageVisit(fullpath);
	}
	else
	{
		Error404(client);
	};
};



#define SDL_main main
int main ()
{
	if (HTTP_Init()) return -1;
	dbInit();



	/// Listen for HTTP traffic on port 80.
	StartListening(80,ServeClient_HTTP,HandleRequest);

	while (1)
	{
		SDL_Delay(DeathCheckInterval);
		if (!SDL_AtomicGet(&ListenersActive)) ServerShouldDie = 1;
		if (ServerShouldDie)
		{
			printf("Shut down signal received.\n");
			break;
		};
		SMH_StatsStep();
	};



	dbQuit();
	HTTP_Quit();
	return 0;
};

