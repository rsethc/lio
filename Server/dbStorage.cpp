#include "dbStorage.h"

struct MainInfo
{
	size_t gis_buf; // points to gis_capacity GlobalInts
	size_t gis_usage;
	size_t gis_capacity;
};

struct GlobalInt
{
	size_t key_len;
	size_t key_buf; // points to key_len chars
	int64_t value;
};

dbStorage::dbStorage (std::string path)
    : raw_file(path)
    , transactions(&raw_file)
    , allocation(&transactions)
{
	size_t primary = GetPrimaryPointer();
	if (!primary)
	{
		/// Initialize to empty database.

		MainInfo main_info;
		main_info.gis_usage = 0;
		main_info.gis_capacity = 16;
		main_info.gis_buf = Allocate(sizeof(GlobalInt) * main_info.gis_capacity);

		size_t main_info_addr = Allocate(sizeof(main_info));
		SetPrimaryPointer(main_info_addr);
		Write(&main_info,main_info_addr,sizeof(main_info));

		Commit();
	};
};

#include <cstring>
void dbStorage::SetGlobalInt (char* key, int64_t value)
{
	size_t keylen = strlen(key);

	MainInfo main_info;
	size_t main_info_addr = GetPrimaryPointer();
	Read(&main_info,main_info_addr,sizeof(main_info));
	for (size_t i = 0; i < main_info.gis_usage; i++)
	{
		GlobalInt gi;
		size_t gi_addr = main_info.gis_buf + sizeof(gi) * i;
		Read(&gi,gi_addr,sizeof(gi));
		if (gi.key_len == keylen)
		{
			if (ContentMatch(key,gi.key_buf,keylen))
			{
				gi.value = value;
				Write(&gi,gi_addr,sizeof(gi));
				return;
			};
		};
	};

	// Not found: create a new GlobalInt.
	if (main_info.gis_usage >= main_info.gis_capacity)
	{
		main_info.gis_capacity <<= 1;
		main_info.gis_buf = Reallocate(main_info.gis_buf,main_info.gis_capacity);
	};
	GlobalInt gi;
	size_t gi_addr = main_info.gis_buf + sizeof(gi) * main_info.gis_usage;
	main_info.gis_usage++;
	Write(&main_info,main_info_addr,sizeof(main_info));
	gi.key_buf = Allocate(keylen);
	gi.key_len = keylen;
	gi.value = value;
	Write(key,gi.key_buf,keylen);
	Write(&gi,gi_addr,sizeof(gi));
};

#include <stdexcept>
int64_t dbStorage::GetGlobalInt (char* key)
{
	size_t keylen = strlen(key);

	MainInfo main_info;
	size_t main_info_addr = GetPrimaryPointer();
	Read(&main_info,main_info_addr,sizeof(main_info));
	for (size_t i = 0; i < main_info.gis_usage; i++)
	{
		GlobalInt gi;
		size_t gi_addr = main_info.gis_buf + sizeof(gi) * i;
		Read(&gi,gi_addr,sizeof(gi));
		if (gi.key_len == keylen)
			if (ContentMatch(key,gi.key_buf,keylen))
				return gi.value;
	};

	// Not found: throw exception.
	throw std::runtime_error("No global integer with this key exists.");
};
