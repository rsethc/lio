#ifndef DBLISTENER_H_INCLUDED
#define DBLISTENER_H_INCLUDED

#include <SDL2/SDL_net.h>

class dbListener
{
	private:
	TCPsocket listen_socket;
	SDLNet_SocketSet socket_set;
	SDL_sem* stop_sem = NULL;
	void Listen ();
	static void ExecListen (dbListener* listener);

	public:
	dbListener (Uint16 port);
	~dbListener ();
	void StopListening ();
};

#endif // DBLISTENER_H_INCLUDED
