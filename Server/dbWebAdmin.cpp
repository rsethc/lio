#include "dbWebAdmin.h"

#include <smh/smh.h>
#include <mime.h>
#include <iostream>
#include <ctime>
#include "dbGlobalDefines.h"

bool dbWebAdmin::sig_exit = false;

// SMH server stats
extern SDL_atomic_t ListenersActive;
extern volatile time_t InitializedAtTime;
extern Uint64 RequestsEverServed;
extern Uint64 RequestsServedLastSecond;
extern SDL_atomic_t OpenConnections;

// Database server stats
extern SDL_atomic_t dbRequestsExecuted;

void ExecutePostData (Client* client, PostData postdata)
{
	IPaddress addr;
	SDLNet_ResolveHost(&addr,"localhost",LISTEN_PORT);
	TCPsocket dbconn = SDLNet_TCP_Open(&addr);
	if (dbconn)
	{
		// Interact with DB on behalf of the web interface.



		SDLNet_TCP_Close(dbconn);
	}
	else
	{
		HTTP_ResponseHeaders(client,"500 Internal Server Error","text/plain");
		HTTP_SendStringContent(client,"Could not establish connection to database.");
	};
};

static void dbWebAdmin::HandleRequest (Client* client)
{
	//std::cout << "processing admin request\n";

	FILE* file;
	if (!strcmp(client->path,"execute"))
	{
		PostData postdata = GetPostData(client,1000);
		ExecutePostData(client,postdata);
	}
	else if (!strcmp(client->path,"admin/shutdown"))
	{
		sig_exit = true;
		Status200(client,NULL);
		SendEmpty(client);
	}
	else if (!strcmp(client->path,"admin/stats"))
	{
		time_t timenow = time(NULL);
		int seconds = timenow - InitializedAtTime;
		int minutes = seconds / 60;
		int hours = minutes / 60;
		int days = hours / 24;
		seconds -= minutes * 60;
		minutes -= hours * 60;
		hours -= days * 24;

		char* uptime_str;
		//asprintf(&str,"%u",SDL_GetTicks() / 1000);
		asprintf(&uptime_str,"%d days, %d hours, %d minutes, %d seconds",days,hours,minutes,seconds);



		char* requests_ever_str;
		asprintf(&requests_ever_str,"%llu",RequestsEverServed);



		char* active_conns_str;
		asprintf(&active_conns_str,"%u",SDL_AtomicGet(&OpenConnections));



		char* requests_last_second_str;
		asprintf(&requests_last_second_str,"%llu",RequestsServedLastSecond);



		char* db_served_total;
		asprintf(&db_served_total,"%llu",SDL_AtomicGet(&dbRequestsExecuted));



		char* full_response_str;
		asprintf(
			&full_response_str,
			"uptime=%s;served=%s;active-connections=%s;served-last-second=%s;db-served-total=%s",
			uptime_str,requests_ever_str,active_conns_str,requests_last_second_str,db_served_total
		);

		free(uptime_str);
		free(requests_ever_str);
		free(active_conns_str);
		free(requests_last_second_str);
		free(db_served_total);



		Status200(client,"text/plain");
		HTTP_SendStringContent(client,full_response_str);

		free(full_response_str);
	}
	else
	{
		//std::cout << client->path << " -> ";
		if (!*client->path) client->path = "index.html";
		//std::cout << client->path << " -> ";

		std::string fullpath = "AdminPages/";
		fullpath += client->path;
		//std::cout << fullpath << "\n";

		file = fopen(fullpath.c_str(),"rb");
		if (file)
		{
			Status200(client,DeduceMimeType(client->path));
			HTTP_SendFile(client,file);
		}
		else
		{
			Error404(client);
		};
	};
};

void dbWebAdmin::AdminMain ()
{
	while (1)
	{
		SDL_Delay(DeathCheckInterval);
		if (!SDL_AtomicGet(&ListenersActive))
			throw std::runtime_error("Failed to establish admin listener");
		if (sig_exit)
			break;
		SMH_StatsStep();
	};

	std::cout << "Received stop signal from web admin\n";

	HTTP_Quit();
	sig_exit = true;
};

void dbWebAdmin::ExecAdminMain (dbWebAdmin* admin) { admin->AdminMain(); };

dbWebAdmin::dbWebAdmin (Uint16 port)
{
	if (HTTP_Init())
		throw std::runtime_error("Failed to initialize SMH server");

	StartListening(port,ServeClient_HTTP,HandleRequest);

	watcher_thread = SDL_CreateThread(ExecAdminMain,"Web Admin Watcher",this);
};

dbWebAdmin::~dbWebAdmin ()
{
	SDL_WaitThread(watcher_thread,NULL);
};
