#ifndef DBSTORAGE_H_INCLUDED
#define DBSTORAGE_H_INCLUDED

#include <cstdio>
#include <cstdint>

#include "../DBLib/BasicFile.h"
#include "../DBLib/AllocatingFile.h"

class dbStorage
{
    BasicFile raw_file;
    TransactionalFile transactions;
    AllocatingFile allocation;
	inline void Read (void* buf, size_t start, size_t len) { transactions.Read(buf,start,len); };
	inline void Write (void* buf, size_t start, size_t len) { transactions.Write(buf,start,len); };
	inline size_t Allocate (size_t len) { return allocation.Allocate(len); };
	inline void Release (size_t addr) { allocation.Release(addr); };
	inline size_t Reallocate (size_t addr, size_t len) { return allocation.Reallocate(addr,len); };
	inline void SetPrimaryPointer (size_t pos) { allocation.SetPrimaryPointer(pos); };
	inline size_t GetPrimaryPointer () { return allocation.GetPrimaryPointer(); };
	inline bool ContentMatch (void* buf, size_t start, size_t len) { return transactions.ContentMatch(buf,start,len); };

	public:
	dbStorage (std::string path);
	inline void Commit () { transactions.Commit(); };
	inline void Revert () { transactions.Revert(); };
	void SetGlobalInt (char* key, int64_t value);
	int64_t GetGlobalInt (char* key);
};

#endif // DBSTORAGE_H_INCLUDED
