#include <SDL2/SDL.h>
#include <SDL2/SDL_net.h>
#include "dbListener.h"
#include "dbWebAdmin.h"
#include <iostream>
#include "dbGlobalDefines.h"
#include "dbStorage.h"
#include "dbConnection.h"

dbStorage* MainStorage;
SDL_mutex* EntireDatabaseMutex;

void ServerMain ()
{
	dbWebAdmin admin(ADMIN_PORT);
	dbListener listener(LISTEN_PORT);

	EntireDatabaseMutex = SDL_CreateMutex();
	MainStorage = new dbStorage("db"); // Open Database

	while (!admin.sig_exit) SDL_Delay(1000);

	dbConnection::CloseAll();

	delete MainStorage; // Close Database
	SDL_DestroyMutex(EntireDatabaseMutex);
};

#define SDL_main main

struct Globals
{
	int x,y,z;

	int a_len;
	size_t a_pos;
};
#include <cstring>

#include "StorageUnitTests.h"
int main ()
{
	StorageUnitTests();

	if (SDL_Init(0) < 0)
		throw std::runtime_error("Failed to initialize SDL");
	if (SDLNet_Init())
		throw std::runtime_error("Failed to initialize SDL_net");

	ServerMain();

	SDLNet_Quit();
	SDL_Quit();
	return 0;
};
