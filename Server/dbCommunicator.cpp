#include "dbCommunicator.h"

#include "dbStorage.h"
extern dbStorage* MainStorage;

static void dbCommunicator::ExecCommunicate (dbCommunicator* communicator)
{
	communicator->Communicate();
	delete communicator;
};

dbCommunicator::dbCommunicator (TCPsocket socket) : conn(socket)
{
	SDL_DetachThread(SDL_CreateThread(ExecCommunicate,"Communication",this));
};

#include <dbRequestTypes.h>

class ProtocolViolation : public std::runtime_error
{
	public:
	ProtocolViolation (std::string text) : std::runtime_error(text) {};
};

SDL_atomic_t dbRequestsExecuted = {0};
extern SDL_mutex* EntireDatabaseMutex;
/// In the future, allow more fine-grained locking to improve performance etc.

void dbCommunicator::db_begin ()
{
	if (owns_lock)
	{
		conn.WriteSint32(ERR_ALREADY_LOCKED);
	}
	else
	{
		SDL_LockMutex(EntireDatabaseMutex);
		owns_lock = true;
		conn.WriteSint32(REQUEST_OK);
	};
};
void dbCommunicator::RelinquishLock ()
{
	owns_lock = false;
	SDL_UnlockMutex(EntireDatabaseMutex);
};
void dbCommunicator::db_commit ()
{
	if (owns_lock)
	{
		MainStorage->Commit();
		RelinquishLock();
		conn.WriteSint32(REQUEST_OK);
	}
	else
	{
		conn.WriteSint32(ERR_WASNT_LOCKED);
	};
};
void dbCommunicator::db_revert ()
{
	if (owns_lock)
	{
		MainStorage->Revert();
		RelinquishLock();
		conn.WriteSint32(REQUEST_OK);
	}
	else
	{
		conn.WriteSint32(ERR_WASNT_LOCKED);
	};
};
void dbCommunicator::db_get_global_int ()
{
	Sint32 name_len = conn.ReadSint32();
	char* name = malloc(name_len + 1);
	name[name_len] = 0;
	conn.Read(name,name_len);
	if (owns_lock)
	{
		try
		{
			int64_t value = MainStorage->GetGlobalInt(name);
			conn.WriteSint32(REQUEST_OK);
			conn.WriteSint64(value);
		}
		catch (std::runtime_error err)
		{
			// should clean this up but we'll just assume it means not found
			conn.WriteSint32(ERR_NAME_NOT_FOUND);
		};
	}
	else
	{
		conn.WriteSint32(ERR_WASNT_LOCKED);
	};
	free(name);
};
void dbCommunicator::db_set_global_int ()
{
	//printf("\twait name len\n");
	Sint32 name_len = conn.ReadSint32();
	char* name = malloc(name_len + 1);
	name[name_len] = 0;
	//printf("\twait name\n");
	conn.Read(name,name_len);
	//printf("\twait value\n");
	Sint64 value = conn.ReadSint64();
	//printf("\tcheck lock\n");
	if (owns_lock)
	{
		MainStorage->SetGlobalInt(name,value);
		conn.WriteSint32(REQUEST_OK);
		//printf("\tsuccess\n");
	}
	else
	{
		// You are required to "Begin" (acquire lock) before
		// performing ANY write operations!
		conn.WriteSint32(ERR_WASNT_LOCKED);
		//printf("\tfailure\n");
	};
	free(name);
	//printf("set_global_int is complete\n");
};
dbCommunicator::RequestHandlerDecider::RequestHandlerDecider ()
{
	handlers[DB_BEGIN] = db_begin;
	handlers[DB_COMMIT] = db_commit;
	handlers[DB_REVERT] = db_revert;
	handlers[DB_GET_GLOBAL_INT] = db_get_global_int;
	handlers[DB_SET_GLOBAL_INT] = db_set_global_int;

	// Make sure all handlers were assigned,
	// if any are still zero then we left one out in the above code.
	for (int i = 0; i < DB_N_REQUEST_TYPES; i++)
		if (!handlers[i])
			throw std::runtime_error("please update the RequestHandlerDecider assignments");
};
dbCommunicator::RequestHandler dbCommunicator::RequestHandlerDecider::Decide (Uint32 type)
{
	//std::cout << "Received request type " << request_type << "\n";
	if (type >= DB_N_REQUEST_TYPES)
		throw ProtocolViolation("handler ID is not valid");
	return handlers[type];
};
static dbCommunicator::RequestHandlerDecider dbCommunicator::decider;


void dbCommunicator::Communicate ()
{
	//std::cout << "communicating with a socket" << '\n';
	try
	{
		if (conn.ReadSint32() != DB_MAGIC)
		{
			throw std::runtime_error("failed magic");
		};

		if (conn.ReadSint32() != DB_PROTOCOL_CHECK)
		{
			conn.WriteSint32(ERR_PROTOCOL_MISMATCH);
			throw std::runtime_error("protocol mismatch");
		}
		else
		{
			conn.WriteSint32(REQUEST_OK);
		};

		//std::cout << "Protocol check passed.\n";

		while (true)
		{
			RequestHandler handler = decider.Decide(conn.ReadSint32());
			(this->*handler)();
			SDL_AtomicIncRef(&dbRequestsExecuted);
		};
	}
	catch (ProtocolViolation error)
	{
		std::cout << "PROTOCOL VIOLATION: " << error.what() << "\n";
	}
	catch (std::runtime_error error)
	{
		std::cout << "Other error: " << error.what() << "\n";
	};
};

dbCommunicator::~dbCommunicator ()
{
	if (owns_lock)
		RelinquishLock();
};
