#include <iostream>
bool AllOK = true;
struct Test
{
	virtual char* name () { return "[Unnamed Test]"; };
	virtual bool test () { return false; };
	void DoTest ()
	{
		if (!test())
		{
			std::cout << "Test Failed: \"" << name() << "\"\n";
			AllOK = false;
		};
	};
};
//#define TestName(name) char* name () { return NULL; } //return name; }
class TrueTest : public Test
{
	//TestName("True Test");
	char* name () { return "True Test"; };
	bool test () { return true; };
};
class FalseTest : public Test
{
	char* name () { return "False Test"; };
	bool test () { return false; };
};

Test* tests [] =
{
	new TrueTest(),
//	new FalseTest(),

};

void StorageUnitTests ()
{
	int n_tests = sizeof(tests) / sizeof(Test);
	for (int i = 0; i < n_tests; i++)
	{
		Test* test = tests[i];
		test->DoTest();
		delete test;
	}
	if (!AllOK) throw std::runtime_error("Unit tests for storage failed.");

	/*
	dbStorage db("db");
	db.SetGlobalInt("rax",100);
	db.SetGlobalInt("eax",-400);
	db.Commit();
	db.SetGlobalInt("eax",323);
	db.Revert();
	std::cout << "rax is " << db.GetGlobalInt("rax") << "\n";
	std::cout << "eax is " << db.GetGlobalInt("eax") << "\n";

	return;
	*/





	//TransactionalFile tr("tr");
	//tr.Write("Good morning.",0,13);
	//tr.Commit();
	//tr.Write("hi",0,2);
	//tr.Revert();
	//return;
//	AllocatingFile file("main_db");
	//printf("%llX\n",file.Allocate(40));
/*	char* hello = "Hello!";
	Globals globals;
	globals.x = 10;
	globals.y = 200;
	globals.z = 4000;
	globals.a_len = strlen(hello);
	globals.a_pos = file.Allocate(globals.a_len);
	printf("%llX\n",globals.a_pos);
	size_t mainpos = file.Allocate(sizeof(Globals));
	file.Write(hello,globals.a_pos,globals.a_len);
	file.Write(&globals,mainpos,sizeof(Globals));
	file.SetPrimaryPointer(mainpos);
	file.ProduceStreamDumps("main_db dump");
*/
/*
	for (int i = 0; i < 10; i++)
	{
		size_t got_1 = file.Allocate(rand() % 500);
		size_t got_2 = file.Allocate(rand() % 500);
		size_t got_3 = file.Allocate(rand() % 500);
		printf("%llX, %llX, %llX\n",got_1,got_2,got_3);
		file.Release(got_1);
		file.Release(got_2);
		file.Release(got_3);
	};

	return;
*/
	//TransactionalFile transactional("transactional");
	//std::cout << "original size: " << transactional.filesize << " bytes\n";
	/*transactional.Write("ouch",0,4);
	transactional.Commit();
	transactional.Write("goodbye",0,7);
	transactional.Write("hello",0,5);
	transactional.Write("Fly!",1,4);
	transactional.Commit();*/
/*
	//dbStorage storage("main_db");
	TransactionalFile storage("main_db");
	storage.Write("0123456789",0,10);
	printf("-- commit --\n");
	storage.Commit();
	storage.Write("ABCDEFG",2,7);
	printf("-- revert --\n");
	storage.Revert();
	/ *printf("write1\n");
	storage.Write("0123456789",0,10);
	printf("commit\n");
	storage.Commit();
	printf("write2\n");
	storage.Write("54321",0,5);
	printf("write3\n");
	storage.Write("ABCDEFG",1,7);
	printf("segfault\n");* /
//	storage.Commit();
//	*(char*)0 = 1;//segfault
*/
	//dbStorage storage("main_db");
	/*TransactionalFile storage("main_db");
	return;
	//for (int i = 0; i < 50; i++)
	{
		storage.Write("Ok..",0,4);
		storage.Write("Hello!",0,6);
		storage.Write("Goodbye",0,7);
		storage.Commit();
		storage.Write("@$@$",3,4);
		storage.Commit();
		storage.Revert();
	};
	*(char*)0 = 1; // segfault*/
/*
	InterleavedFile inter ("interleaved.txt");
	char* a = "Lorem ipsum dolor something idk.\n";
	size_t a_len = strlen(a);
	char* b = "Ay wassup goomba\n";
	size_t b_len = strlen(b);
	char* c = "mExIcO wIlL pAy FoR tHe WaLl\n";
	size_t c_len = strlen(c);
	char* d = "Just 400 easy payments of $29.95, plus shipping and handling.\n";
	size_t d_len = strlen(d);
	inter.DataWrite(b,0,b_len);
	inter.ControlWrite(a,0,a_len);
	inter.DataWrite(d,b_len,d_len);
	inter.ControlWrite(c,a_len,c_len);

	char* a_back = malloc(a_len+1); a_back[a_len] = 0;
	char* b_back = malloc(b_len+1); b_back[b_len] = 0;
	char* c_back = malloc(c_len+1); c_back[c_len] = 0;
	char* d_back = malloc(d_len+1); d_back[d_len] = 0;

	inter.DataRead(b_back,0,b_len);
	inter.ControlRead(a_back,0,a_len);
	inter.DataRead(d_back,b_len,d_len);
	inter.ControlRead(c_back,a_len,c_len);

	printf("\"%s\"->\"%s\"\n",a,a_back);
	printf("\"%s\"->\"%s\"\n",b,b_back);
	printf("\"%s\"->\"%s\"\n",c,c_back);
	printf("\"%s\"->\"%s\"\n",d,d_back);

return;

*/



};
