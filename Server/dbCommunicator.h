#ifndef DBCOMMUNICATOR_H_INCLUDED
#define DBCOMMUNICATOR_H_INCLUDED

#include "dbConnection.h"
#include "dbRequestTypes.h"
#include <iostream>

class dbCommunicator
{
	private:
	bool owns_lock = false; // This communicator owns the exclusive database lock.
	dbConnection conn;
	void Communicate ();
	static void ExecCommunicate (dbCommunicator* communicator);

	// Communication handling functions
	void RelinquishLock ();
	void db_begin ();
	void db_commit ();
	void db_revert ();
	void db_get_global_int ();
	void db_set_global_int ();



	typedef void (dbCommunicator::*RequestHandler) ();
	class RequestHandlerDecider
	{
		RequestHandler handlers [DB_N_REQUEST_TYPES] = {}; // default to all zeroes

		public:
		RequestHandlerDecider ();
		RequestHandler Decide (Uint32 type);
	};
	static RequestHandlerDecider decider;






	public:
	dbCommunicator (TCPsocket socket);
	~dbCommunicator ();
};

#endif // DBCOMMUNICATOR_H_INCLUDED
