#ifndef DBCONNECTION_H_INCLUDED
#define DBCONNECTION_H_INCLUDED

#include <SDL2/SDL_net.h>

class dbConnection
{
	private:
	TCPsocket socket;
	SDLNet_SocketSet socket_set;
	void MakeSet ();
	void GoActive ();
	static volatile bool all_exit;
	static SDL_atomic_t currently_active;

	public:
	static void CloseAll ();
	dbConnection (TCPsocket socket);
	dbConnection (char* hostname, Uint16 hostport);
	~dbConnection ();

	void Read (void* buf, size_t len);
	void Write (void* buf, size_t len);

	void WriteSint64 (Sint64 n);
	Sint64 ReadSint64 ();
	void WriteSint32 (Sint32 n);
	Sint32 ReadSint32 ();
	void WriteSint16 (Sint16 n);
	Sint16 ReadSint16 ();
	void WriteSint8 (Sint8 n);
	Sint8 ReadSint8 ();
};

#include <stdexcept>

class dbConnectionLost : public std::runtime_error
{
	public:
	inline dbConnectionLost () : std::runtime_error("The network connection was closed.") {};
};

#endif // DBCONNECTION_H_INCLUDED
