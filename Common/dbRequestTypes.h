#ifndef DBREQUESTTYPES_H_INCLUDED
#define DBREQUESTTYPES_H_INCLUDED

#define DB_MAGIC 0xDA7ABA5E

#define DB_PROTOCOL_CHECK 2

/// Request Types: Client -> Server
enum
{
	DB_BEGIN,
	DB_COMMIT,
	DB_REVERT,

	DB_GET_GLOBAL_INT,
	DB_SET_GLOBAL_INT,



	DB_N_REQUEST_TYPES
};

/// Request Error Codes: Server -> Client
enum
{
	REQUEST_OK,



	ERR_DISALLOWED, // permissions violation
	ERR_NAME_NOT_FOUND, // nothing with this key exists yet
	ERR_PROTOCOL_MISMATCH, // start-up handshake failed
	ERR_WASNT_LOCKED, // no data operations are allowed unless you've started a transaction
	ERR_ALREADY_LOCKED, // don't do Begin twice without a Revert or Commit in between
};

#endif // DBREQUESTTYPES_H_INCLUDED
