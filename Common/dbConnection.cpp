#include "dbConnection.h"
#include <stdexcept>
#include <iostream>

void dbConnection::GoActive ()
{
	SDL_AtomicIncRef(&currently_active);
	if (all_exit)
	{
		SDL_AtomicDecRef(&currently_active);
		throw std::runtime_error("Cannot initialize socket after CloseAll");
	};
};

void dbConnection::MakeSet ()
{
	socket_set = SDLNet_AllocSocketSet(1);
	SDLNet_TCP_AddSocket(socket_set,socket);
};

dbConnection::dbConnection (TCPsocket socket)
{
	GoActive();
	this->socket = socket;
	MakeSet();
};

dbConnection::dbConnection (char* hostname, Uint16 hostport)
{
	GoActive();
	std::cout << "Connecting to " << hostname << ":" << hostport << "\n";
	IPaddress addr;
	SDLNet_ResolveHost(&addr,hostname,hostport);
	socket = SDLNet_TCP_Open(&addr);
	//std::cout << "Socket Handle = " << socket << "\n";
	if (!socket) throw std::runtime_error("Could not establish TCP socket.");
	MakeSet();
};

dbConnection::~dbConnection ()
{
	//std::cout << "closing socket " << socket << "\n";
	SDLNet_FreeSocketSet(socket_set);
	SDLNet_TCP_Close(socket);
	SDL_AtomicDecRef(&currently_active);
};

volatile bool dbConnection::all_exit = false;
SDL_atomic_t dbConnection::currently_active = {0};
static void dbConnection::CloseAll ()
{
	all_exit = true;
	while (SDL_AtomicGet(&currently_active)) SDL_Delay(1000);
};
void dbConnection::Read (void* buf, size_t len)
{
	//std::cout << "socket is " << socket << "\n";
	while (len)
	{
		do
		{
			if (all_exit)
				throw std::runtime_error("all connections must close");
		}
		while (!SDLNet_CheckSockets(socket_set,1000));
		int received = SDLNet_TCP_Recv(socket,buf,len);
		//std::cout << received << " bytes\n";
		if (received <= 0) throw std::runtime_error("socket is dead");
		buf += received;
		len -= received;
	};
};

void dbConnection::Write (void* buf, size_t len)
{
	SDLNet_TCP_Send(socket,buf,len);
};

void dbConnection::WriteSint64 (Sint64 n)
{
	SDL_SwapLE64(n);
	Write(&n,8);
};
Sint64 dbConnection::ReadSint64 ()
{
	Sint64 n;
	Read(&n,8);
	SDL_SwapLE64(n);
	return n;
};
void dbConnection::WriteSint32 (Sint32 n)
{
	SDL_SwapLE32(n);
	Write(&n,4);
};
Sint32 dbConnection::ReadSint32 ()
{
	Sint32 n;
	Read(&n,4);
	SDL_SwapLE32(n);
	return n;
};
void dbConnection::WriteSint16 (Sint16 n)
{
	SDL_SwapLE16(n);
	Write(&n,2);
};
Sint16 dbConnection::ReadSint16 ()
{
	Sint16 n;
	Read(&n,2);
	SDL_SwapLE16(n);
	return n;
};
void dbConnection::WriteSint8 (Sint8 n)
{
	Write(&n,1);
};
Sint8 dbConnection::ReadSint8 ()
{
	Sint8 n;
	Read(&n,1);
	return n;
};
