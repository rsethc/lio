#include <dbInteraction.h>
#include "account_names.h"
void Spam ()
{
	dbLink link;
	while (true)
	{
		char* from = Accounts[rand() % N_Accounts];
		char* to = Accounts[rand() % N_Accounts];
		if (from == to) continue;
		int amount = rand() % (StartingBalance * 3);
		link.Begin();
		Sint64 from_bal = link.GetInt(from);
		Sint64 to_bal = link.GetInt(to);
		link.SetInt(from,from_bal - amount);
		link.SetInt(to,to_bal + amount);
		if (link.GetInt(from) < 0)
		{
			printf("Oops, cancel that\n");
			link.Revert();
		}
		else
		{
			printf("Transaction succeeded\n");
			link.Commit();
		};
	};
};
#define SDL_main main
int main ()
{
	dbInit();
	Spam();
	dbQuit();
	return 0;
};
