/// This is the file intended to be directly included by an application.

#ifndef DBINTERACTION_H_INCLUDED
#define DBINTERACTION_H_INCLUDED

void dbInit ();
void dbQuit ();

#include "dbLink.h"

#endif // DBINTERACTION_H_INCLUDED
