#ifndef DBICONNECTION_H_INCLUDED
#define DBICONNECTION_H_INCLUDED

#include <SDL2/SDL.h>
#include <SDL2/SDL_net.h>
#include "dbConnection.h"
//#include "dbResult.h"

class dbLink
{
	dbConnection connection;

	public:
	dbLink (char* hostname = "localhost", Uint16 hostport = 458);
	~dbLink ();
	void Begin (); // Acquire exclusive lock on the database.
	void Commit (); // Commit changes and release exclusive lock.
	void Revert (); // Cancel changes and release exclusive lock.
	Sint64 GetInt (char* name);
	//dbResult GetInt (char* name);
	void SetInt (char* name, Sint64 value);
};

#endif // DBICONNECTION_H_INCLUDED
