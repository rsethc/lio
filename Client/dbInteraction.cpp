#include "dbInteraction.h"

// Allows multiple calls to dbInit/dbQuit to be "layered" without causing trouble.
int dbInitCount = 0;

void dbInit ()
{
	if (dbInitCount++) return; // Was already initialized.

	if (SDL_Init(0) < 0)
		throw std::runtime_error("Failed to initialize SDL");
	if (SDLNet_Init())
		throw std::runtime_error("Failed to initialize SDL_net");
};

void dbQuit ()
{
	if (--dbInitCount) return; // More calls to dbQuit will follow.

	SDLNet_Quit();
	SDL_Quit();
};
