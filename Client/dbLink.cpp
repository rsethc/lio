#include "dbLink.h"

#include "dbRequestTypes.h"

dbLink::dbLink (char* hostname, Uint16 hostport) : connection(hostname,hostport)
{
	connection.WriteSint32(DB_MAGIC);
	connection.WriteSint32(DB_PROTOCOL_CHECK);
	Sint32 error = connection.ReadSint32();
	if (error) throw error;
	//Uint32 protocol_check = DB_PROTOCOL_CHECK;
	//connection.Write(&protocol_check,4);
};
dbLink::~dbLink ()
{

};
void dbLink::Begin ()
{
	printf("Begin ");
	connection.WriteSint32(DB_BEGIN);
	Sint32 error = connection.ReadSint32();
	if (error) throw std::runtime_error("failed to begin");
	printf("OK\n");
};
void dbLink::Commit ()
{
	printf("Commit ");
	connection.WriteSint32(DB_COMMIT);
	Sint32 error = connection.ReadSint32();
	if (error) throw std::runtime_error("failed to commit");
	printf("OK\n");
};
void dbLink::Revert ()
{
	printf("Revert ");
	connection.WriteSint32(DB_REVERT);
	Sint32 error = connection.ReadSint32();
	if (error) throw std::runtime_error("failed to revert");
	printf("OK\n");
};
Sint64 dbLink::GetInt (char* name)
{
	printf("GetInt ");
	connection.WriteSint32(DB_GET_GLOBAL_INT);
	Sint32 name_len = strlen(name);
	connection.WriteSint32(name_len);
	connection.Write(name,name_len);
	Sint32 error = connection.ReadSint32();
	if (error) throw std::runtime_error("failed to get global int");
	Sint64 value = connection.ReadSint64();
	printf("OK\n");
	return value;
};
void dbLink::SetInt (char* name, Sint64 value)
{
	printf("SetInt ");
	connection.WriteSint32(DB_SET_GLOBAL_INT);
	Sint32 name_len = strlen(name);
	connection.WriteSint32(name_len);
	connection.Write(name,name_len);
	connection.WriteSint64(value);
	Sint32 error = connection.ReadSint32();
	if (error) throw std::runtime_error("failed to set global int");
	printf("OK\n");
};
