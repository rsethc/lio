INCLUDES += -I.
INCLUDES += -I/usr/include -I/usr/local/include
CFLAGS += -w -O2 -fomit-frame-pointer -s -fPIC -w
CXXFLAGS += $(CFLAGS) -fpermissive

NAMEVER = package

PRODUCT = liblio.a

ALL_PRODUCTS = $(PRODUCT_LIBS) $(PRODUCT_EXECUTABLES)

LIO_OBJS = $(patsubst %.cpp,%.o,$(shell find DBLib -iname '*.cpp'))

all: $(PRODUCT)

install: all
	install $(PRODUCT) /usr/local/lib
	mkdir -p /usr/local/include/DBLib
	install -D $(shell find DBLib -iname '*.h') /usr/local/include/DBLib

liblio.a: $(LIO_OBJS)
	ar -cr $@ $^

%.o: %.cpp
	$(CXX) -c $(CXXFLAGS) $(INCLUDES) $< -o $@

clean:
	@rm -rvf $(shell find . -iname '*.o') *.a
