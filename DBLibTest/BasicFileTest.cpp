#include <DBLib/LinearFile.h>
#include <stdlib.h>
#define maxwrite 100000
int LinearFileTest (LinearFile* file)
{
	char written [maxwrite];
	int maxoffset = 0;
	for (int i = 0; i < 10; i++)
	{
		int count = rand() % maxwrite;
		int offset = rand() % maxwrite;
		for (int i = 0; i < count; i++)
			written[i] = rand();
		file->Write(written,offset,count);
		char gotback [maxwrite];
		file->Read(gotback,offset,count);
		for (int i = 0; i < count; i++)
			if (written[i] != gotback[i]) return -1;
	}
	return 0;
};

#include <DBLib/BasicFile.h>
int BasicFileTest ()
{
	//remove("basicfile_doesntexist");
	//remove("basicfile");

	try
	{
		BasicFile file("basicfile_doesntexist",false);
		printf("[!] Allowed creating file when create_allowed was false.\n");
	}
	catch (...) {}

	BasicFile file("basicfile");
	BasicFile samefile("basicfile");
	char* asdf = "asdfghjk12345678";
	char buf [16];

	file.Write(asdf,0,16);
	file.Read(buf,0,16);
	for (int i = 0; i < 16; i++) if (asdf[i] != buf[i]) { printf("[!] Write-then-read [0,16] failed\n"); break; }

	file.Write(asdf,8,16);
	file.Read(buf,8,16);
	for (int i = 0; i < 16; i++) if (asdf[i] != buf[i]) { printf("[!] Write-then-read [8,16] failed\n"); break; }

	file.Flush();
	samefile.Read(buf,8,16);
	for (int i = 0; i < 16; i++) if (asdf[i] != buf[i]) { printf("[!] Flush-then-read [8,16] failed\n"); break; }

	if (LinearFileTest(&file)) printf("[!] basicfile linearfiletest FAILED\n");
};
